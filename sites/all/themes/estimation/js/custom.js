(jQuery)(function($) {
    
jQuery(document).ready(function ($) {



// FLICKR FEED

    $('.flickr-images').append('<div class="col-row"><ul class="stream-portfolio cf"></ul></div>');

    $('.flickr-images ul').jflickrfeed({
        limit: 6,
        qstrings: {
            id: '99771506@N00', // enter Flickr ID            
            tags: '' // Displays images with selected tags (optional)
        },

        itemTemplate: '<li><a href="{{image_b}}" title="{{title}}" data-nivo-rel="nivoLightbox" data-lightbox-gallery="flickr-gallery"><img src="{{image_s}}" alt="{{title}}" /></a><span class="stream-portfolio-overlay"><i class="fa fa-search"></i></span></li>'

    }, function (data) {
        $('.flickr-images li a').nivoLightbox({
            effect: 'fade'
        });
    });

//SHARING FUNCTIONALITY - SHARRRE.JS

    $('#twitter').sharrre({
        share: {
            twitter: true
        },
        template: '<div class="box" href="#"><div class="share"><i class="fa fa-twitter"></i></div></div>',
        enableHover: false,
        enableTracking: true,
        buttons: {
            twitter: {
                via: 'DDamir'
            }
        },
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('twitter');
        }
    });

    $('#facebook').sharrre({
        share: {
            facebook: true
        },
        template: '<div class="box" href="#"><div class="share"><i class="fa fa-facebook"></i></div></div>',
        enableHover: false,
        enableTracking: true,
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('facebook');
        }
    });


//SEARCH BOX

    $('#toggle-search').click(function (event) {
        event.stopPropagation();
        event.preventDefault();
        if ($(this).hasClass('search-open')) {
            $('.search-panel').slideDown(300);
            //$('#header-search-box').focus();
            $(this).removeClass('search-open').addClass('search-close');
        } else if ($(this).hasClass('search-close')) {
            $('.search-panel').slideUp(300);
            $(this).removeClass('search-close').addClass('search-open');
        }

    });

    $('body').click(function (evt) {

        if (evt.target.id == 'edit-search-block-form--2')
            return;

        $('.search-panel').slideUp(300);
        $('#toggle-search').removeClass('search-close').addClass('search-open');

    });



//ADDING AN ARROW TO THE LINKS WITH THE DROPDOWN MENU

    var navLink = $('#main-navigation ul li ul li');
 
    navLink.each(function() {   
            
        if( $(this).find('ul').length ){
            var a = $(this).find('a').eq(0);

            $('<i class="fa fa-angle-right"></i>').appendTo(a);
        }
            
    });    

//SCROLL TO TOP TRIGGER

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scroll-top').fadeIn('slow');
        } else {
            $('.scroll-top').fadeOut('slow');
        }
    }); 

    $('.scroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('.content-slider').parent('.container-slider').hover(function() {        
        $(this).find('.flex-direction-nav').animate({
            opacity: '1'
        }, 200);
    },
    function() {
         $(this).find('.flex-direction-nav').animate({
            opacity: '0'
        }, 200);
    });

//LOCAL LINK FUNCTION   

    $('.local').click(function () {
        var ele = $(this);
        var location = $(ele).attr('href');

        $('html, body').animate({
            scrollTop: $(location).offset().top
        }, 1000);
    });

//INITIALIZES NIVO LIGHTBOX PLUGIN

    $('a[data-nivo-rel^="nivoLightbox"]').nivoLightbox({
        effect: 'fade'
    });

//INITIALIZES FLEXSLIDER CAROUSELS

    $('.carousel-container').flexslider({
        animation: 'slide',
        slideshow: false,
        animationLoop: false,
        controlNav: false,
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>',
        itemWidth: 234,
        itemMargin: 24
    });

    $('.carousel-full').parent('.col-row').css({marginRight : '0'});

//INITIALIZES FLEXSLIDER IMAGE GALLERIES

    $('.image-gallery').flexslider({
        animation: 'fade',
        animationLoop: false,
        controlNav: false,
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>',
    });

    $('.image-gallery-thumbs').flexslider({
        animation: 'fade',
        slideshow: false,
        animationLoop: false,
        directionNav: false,
        controlNav: 'thumbnails',
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>',
    });

//INITIALIZES FLEXSLIDER CONTENT SLIDER

    $('.content-slider').flexslider({
        animation: 'fade',
        animationLoop: false,
        controlNav: false,
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>',
    });

// vTICKER FOR ROTATING TWEETS

//    $(function () {
  //      $('.twitter-feed').vTicker('init', {
   //         speed: 400,
   //         pause: 5000,
    //        padding: 15
    //    });
   // });

//ACCORDIONS AND TABS

    $('.accordion').accordion({
        collapsible: true,
        heightStyle: 'content'
    });

    $('.tabs-top, .tabs-top-2').tabs({
        show: {
            effect: 'fadeIn',
            duration: 500
        }
    });

    $('.tabs-side, .tabs-side-2').tabs({
        show: {
            effect: 'fadeIn',
            duration: 500
        }
    }).addClass('ui-tabs-vertical ui-helper-clearfix');

    $('.tabs-side li').removeClass('ui-corner-top').addClass('ui-corner-left');

//CREATING RESPONSIVE NAVIGATION (DROPDOWN MENU)    

    $('<div class="responsive-nav" />').appendTo('#header');

    var $navigation = $('<select />');
    $('<option />', {
        'selected': 'selected',
        'value': '',
        'text': 'Main Navigation'
    }).appendTo($navigation);

    $navigation.appendTo('.responsive-nav');

    $('#main-navigation ul li a').each(function () {

        var navUrl = $(this).attr('href');
        var navText = $(this).clone().children().remove().end().text();

        if ($(this).parents('li').length == 2) {
            navText = '- ' + navText;
        }
        if ($(this).parents('li').length == 3) {
            navText = '-- ' + navText;
        }
        if ($(this).parents('li').length > 3) {
            navText = '--- ' + navText;
        }

        $('<option />', {
            'value': navUrl,
            'text': navText
        }).appendTo($navigation)
    });

    field_id = '.responsive-nav select';

    $(field_id).change(function () {
        value = $(this).attr('value');
        window.location = value;
    });

//INITIALIZES THE PERSISTENT TOP NAVIGATION BAR ON SMALLER SCREENS

    $('.responsive-nav').waypoint('sticky', {
        stuckClass: 'stuck',
        offset: -150
    });

//NOTIFICATION BOXES

    $('.info-close, .remove-item').click(function () {

        var parent = $(this).parent();

        $(parent).slideUp({
            duration: 300
        });
        return false;

    });

//

 

// TOOLTIPS

    $('.ttip-top').tooltip({
        position: {
            my: 'center bottom-15',
            at: 'center top',
            using: function (position, feedback) {
                $(this).css(position);
                $('<div>')
                    .addClass('arrow')
                    .addClass(feedback.vertical)
                    .addClass(feedback.horizontal)
                    .appendTo(this);
            }
        }
    });

    $('.ttip-bottom').tooltip({
        position: {
            my: 'center bottom+40',
            at: 'center bottom',
            using: function (position, feedback) {
                $(this).css(position);
                $('<div>')
                    .addClass('arrow')
                    .addClass(feedback.vertical)
                    .addClass(feedback.horizontal)
                    .appendTo(this);
            }
        }
    });

// BLOG IMAGE HOVER

    $('.img-link img').hover(function () {
            $(this).stop().animate({
                opacity: '.7'
            }, 200);
        },
        function () {
            $(this).stop().animate({
                opacity: '1'
            }, 200);
        });


//ISOTOPE SETUP

    // cache container
//    var $portfolio_container = $('#filterable');
 //   var $portfolio_filter = $('.filters li a');
    // filter items when filter link is clicked

//    $portfolio_filter.click(function () {
  //      $portfolio_filter.removeClass('current');
  //      $(this).addClass('current');
   //     var selector = $(this).attr('data-filter');

   //     $portfolio_container.isotope({
   //         filter: selector
   //     });
    //    return false;
   // });

  //  $(window).load(function () {
    //    $('#filterable').isotope({
     //       filter: '*',
     //       layoutMode: 'fitRows'

     //   });
  //  });


//ISOTOPE - SHOP PRODUCTS

  /*  // cache container
    var $shop_container = $('#product-container');
    var $shop_filter = $('#shop-filters li a');
    // filter items when filter link is clicked

    $shop_filter.click(function () {
        $shop_filter.removeClass('current');
        $(this).addClass('current');
        var selector = $(this).attr('data-filter');

        $container.isotope({
            filter: selector
        });
        return false;
    });

    $(window).load(function () {
        $('#product-container').isotope({
            filter: '*',
            layoutMode: 'fitRows'

        });
    });*/



//PRELOADER


    $(window).on('load', function() {
        $('#status').fadeOut('fast'); // will first fade out the loading animation
        $('#preloader').delay(250).fadeOut('slow', function() {
            $(this).addClass('none');
        }); // will fade out the white DIV that covers the website.
        $('body').delay(1000).css({'overflow':'visible', 'height': 'auto'});
    });      

//CONTENT ANIMATIONS    

    //ANIMATE ON SCROLL

    $('.no-touch .animated').waypoint(function () {

        var animation = $(this).attr('data-animation');
        var xposition = $(this).attr('data-xposition');
        var yposition = $(this).attr('data-yposition');
        var delay = $(this).attr('data-animation-delay');

        $(this).addClass(animation, function () {
            $(this).css({
                opacity: '1',
                marginLeft: xposition + 'px',
                marginTop: '-' + yposition + 'px',
                animationDelay: delay + 'ms'
            });
        });

    }, {
        offset: '85%',
        triggerOnce: true
    });

    $('.skillbar').waypoint(function () {
        $('.skillbar').each(function () {
            $(this).find('.skillbar-bar').animate({
                width: $(this).attr('data-percent')
            }, 2000);
        });
    }, {
        offset: '85%'
    });

//INITIALIZES COUNTER PLUGIN    

    $('.timer').waypoint(function () {
        $('.timer').countTo();
    }, {
        offset: '85%',
        triggerOnce: true
    });

//ANIMATE ON HOVER

    $('.animated-hover').each(function () {

        var el = $(this);
        var animation = el.attr('data-animation');

        el.hover(function () {
                el.addClass('animated ' + animation);
            },
            function () {
                setTimeout(function () {
                    el.removeClass('animated ' + animation); 
                }, 1500); 
            });

    });

//SHARE NETWORKS DROPDOWN

    $('.share-networks').hide();

    $('.share-btn').click(function () {

        if ($(this).is('.closed')) {
            $(this).removeClass('closed').addClass('opened').prev('.share-networks').slideDown(500);
            return false;
        } else {
            $(this).removeClass('opened').addClass('closed').prev('.share-networks').slideUp(500);
            return false;
        }

    });
    
    
    //ENABLE MENU ITEM WHEN SECTION IS SHOWN
    // Cache selectors

    $('#superfish-1 li').removeClass('active-trail');
    $('#superfish-1 li a').removeClass('active');
    
    $("a").on("click", function(e) {

        var ref = $(this).attr('href').replace("/#", "#");
        if (ref == '#contact-chat') {
            e.preventDefault();
            LC_API.open_chat_window({source:'minimized'});
        }
        
        if( $(ref).length > 0 ) {
            e.preventDefault();
            
            var hh = 97; // header height
            if( $('body').hasClass('admin-menu')) {
                hh = hh+28; // 28 = admin menu height
            } 
            if(ref == '#footer') {
                $("body, html").animate({ 
                    scrollTop: ( $(ref).offset().top - hh)
                }, 600);
            } else {
                $("body, html").animate({ 
                    scrollTop: ( $(ref).parents('section').offset().top - hh)
                }, 600);
            }
            //console.log('hola');
        }

    });
    
    /*--- Biography ---*/
    $(".showBiography").on("click", function( e ) {
       e.preventDefault();
       $(this).parent().parent().parent().toggleClass('show');
    });
    
    $('.btnHide').on('click',function(){
        $(this).parent().addClass('hide');
    });
    
    $('.userOpen').click(function(e){ e.preventDefault(); });
}); //END of jQuery
 });
 
window.addEventListener("click", openMenu);

function openMenu(event) {
    var classOpen = 'openBlock';
    var classAdded = 'open';
    var elementClicked = event.target;
    if(elementClicked.classList.contains(classOpen)) {
        if (elementClicked.parentNode.classList.contains(classAdded)) {
            elementClicked.parentNode.classList.remove(classAdded);
            jQuery('.userInfoBlock .userForm').slideUp(300);
        } else {
            jQuery('.userInfoBlock .userForm').slideDown(300);
            elementClicked.parentNode.classList.add(classAdded);
        }
        
    } else {
        if (!hasSomeParentTheClass(elementClicked,classAdded)) {
            jQuery('.userInfoBlock .userForm').slideUp(300);
            var classElements = document.querySelectorAll('.'+classOpen);
            for (var classElement of classElements) {
                classElement.parentNode.classList.remove(classAdded);
            }
        }
    }
}

// returns true if the element or one of its parents has the class classname
function hasSomeParentTheClass(element, classname) {
    if (element.parentNode === null) {
        return false;
    }
    if (element.classList.contains(classname))
        return true;
    return element.parentNode && hasSomeParentTheClass(element.parentNode, classname);
}

/*--- Slider - home promotional ---*/


/*--- Slider - testimonial promotional ---*/
jQuery(document).ready(function(){
    if (jQuery('.testimonialsBlock').length > 0) {
        var jcarousel = jQuery('.testimonialsBlock .jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = jQuery(this),
                width = carousel.innerWidth();
                winWidth = jQuery(window).width();
                if (winWidth >= 768) {
                    width = width / 2;
                } else {
                    width = width / 1;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            }).jcarouselSwipe({
                draggable: false
            });

        jQuery('.testimonialsBlock .flex-prev')
            .jcarouselControl({
                target: '-=1'
            });

        jQuery('.testimonialsBlock .flex-next')
            .jcarouselControl({
                target: '+=1'
            });
    }
    jQuery('.menuMobile').on('click',function(){
        jQuery('#superfish-1').toggleClass('show');
    });
    jQuery('#superfish-1 li a').on('click',function(){
        jQuery('#superfish-1').removeClass('show');
    });
});
