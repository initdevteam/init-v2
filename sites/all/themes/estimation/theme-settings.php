<?php

function estimation_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['estimation_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('estimation Theme Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

$form['estimation_settings']['general_settings']['estimation_preloader'] = array(
    '#type' => 'checkbox',
    '#title' => t('Page preloader?'),
    '#default_value' => theme_get_setting('estimation_preloader', 'estimation'),
    '#description' => t("Check for yes!"),
  );
  
  $form['estimation_settings']['general_settings']['theme_color_config']['theme_color_palette'] = array(
    '#type' => 'select',
    '#title' => t('Choose a color palette'),
    '#default_value' => theme_get_setting('theme_color_palette'),
    '#options' => array(
      'tangerine' => t('Tangerine'),
      'soft-blue' => t('Soft Blue'),
      'red' => t('Red'),
      'orange' => t('Orange'),
      'blue' => t('Blue'),
	  'green' => t('Green'),
      'soft-red' => t('Soft Red'),
      'pink' => t('Pink'),
	  'orchid' => t('Orchid'),
	  'light-brown' => t('Ligh Brown'),
	  'light-cyan' => t('Light Cyan'),
	  'dark-cyan' => t('Dark Cyan'),
      'dark-pink' => t('Dark Pink'),
      'deep-blue' => t('Deep Blue'),
      'olive-green' => t('Olive Green'),
    ),
  );

 $form['estimation_settings']['general_settings']['estimation_boxed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Boxed Layout?'),
    '#default_value' => theme_get_setting('estimation_boxed', 'estimation'),
    '#description' => t("Check for yes!"),
  );
  
  $form['estimation_settings']['general_settings']['theme_pattern_config']['theme_bg_pattern'] = array(
    '#type' => 'select',
    '#title' => t('Choose a background pattern'),
    '#default_value' => theme_get_setting('theme_bg_pattern'),
    '#options' => array(
      'none' => t('none'),
      'pattern-1' => t('Pattern-1'),
      'pattern-2' => t('Pattern-2'),
      'pattern-3' => t('Pattern-3'),
      'pattern-4' => t('Pattern-4'),
      'pattern-5' => t('Pattern-5'),
      'pattern-6' => t('Pattern-6'),
      'pattern-7' => t('Pattern-7'),
      'pattern-8' => t('Pattern-8'),
	  'pattern-9' => t('Pattern-9'),
	  'pattern-10' => t('Pattern-10'),
      'pattern-11' => t('Pattern-11'),
      'pattern-12' => t('Pattern-12'),
      'pattern-13' => t('Pattern-13'),
      'pattern-14' => t('Pattern-14'),
      'pattern-15' => t('Pattern-15'),
	  'pattern-16' => t('Pattern-16'),
    ),
  );


$form['estimation_settings']['general_settings']['estimation_bg'] = array(
    '#type' => 'checkbox',
    '#title' => t('Front page content background?'),
    '#default_value' => theme_get_setting('estimation_bg', 'estimation'),
    '#description' => t("Check for yes!"),
  );
}
