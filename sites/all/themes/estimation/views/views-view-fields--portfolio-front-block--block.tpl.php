 <li class="one-fourth animated" data-animation="fadeInUp">
<div class="p-item">
		<figure>
			<div class="overlay-container">
             <?php print $fields['field_portfolio_image_view']->content; ?>

				<div class="overlay">
					<div class="overlay-buttons">
												<?php /*<a href="<?php print $fields['field_portfolio_image_1']->content; ?>" data-nivo-rel="nivoLightbox">
													<i class="fa fa-arrows-alt"></i>
												</a>
												<a class="last" href="<?php print $fields['path']->content; ?>"><i class="fa fa-chain"></i></a> */ ?>
											</div><!-- end overlay-buttons -->
											<div class="overlay-bg"></div><!-- end overlay-bg -->
										</div><!-- end overlay -->

									</div><!-- end overlay-container -->	

									<figcaption>
										<h5><?php print $fields['title']->content; ?></h5>
										<span><?php print $fields['field_portfolio_tags']->content; ?></span>
									</figcaption>
								</figure>
							</div><!-- end p-item -->	
                                    </li>
                                   