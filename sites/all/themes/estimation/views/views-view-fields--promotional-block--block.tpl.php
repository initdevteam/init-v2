<div class="content promotionalBlock" id="promotion-<?php echo $fields['nid']->content; ?>">
    <div class="one-half">
        <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <?php print $fields['field_gallery']->content; ?>
                <p class="jcarousel-pagination"></p>
            </div>
        </div>
    </div>
    <div class="one-half">
        <div class="section-title">
            <h2 class="heading-extra-big"><?php print $fields['title']->content; ?></h2>
            <h3 class="sub-heading"><?php print $fields['field_subtitle']->content; ?></h3>
        </div>
        <div class="bodyBlock">
            <?php print $fields['body']->content; ?>
        </div>
        <a class="button medium-btn" href="<?php print $fields['field_button_link']->content; ?>"><?php print $fields['field_button_text']->content; ?></a>
    </div>
</div>

<script>
jQuery(document).ready(function(){
    if (jQuery('#promotion-<?php echo $fields['nid']->content; ?> .jcarousel > .item-list ul li').length > 1) {
        var jcarousel = jQuery('#promotion-<?php echo $fields['nid']->content; ?> .jcarousel > .item-list');

        jcarousel.on('jcarousel:reload jcarousel:create', function () {
            var carousel = jQuery(this),
            width = carousel.innerWidth();
            carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
        }).jcarousel({
            wrap: 'circular'
        });
        jQuery('#promotion-<?php echo $fields['nid']->content; ?> .jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
            jQuery(this).addClass('active');
        }).on('jcarouselpagination:inactive', 'a', function() {
            jQuery(this).removeClass('active');
        }).on('click', function(e) {
            e.preventDefault();
        }).jcarouselPagination({
            perPage: 1,
            item: function(page) {
                return '<a href="#' + page + '">&nbsp;</a>';
            }
        });
    } else {
        jQuery('#promotion-<?php echo $fields['nid']->content; ?> .jcarousel').addClass('onlyImage');
        jQuery('#promotion-<?php echo $fields['nid']->content; ?> .jcarousel ul li:eq(0)').addClass('animated');
        jQuery('#promotion-<?php echo $fields['nid']->content; ?> .jcarousel ul li:eq(0)').attr('data-animation', 'flipInX');
    }
});
</script>
<div class="clearfix"></div>