<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="animated testimonialsBlock" data-animation="fadeInLeft">	
    <div class="col-row">
        <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul class="slides">
                    <?php foreach ($rows as $id => $row): ?>
                        <?php print $row; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <ul class="flex-direction-nav">
                <li>
                    <a class="flex-prev" href="#" tabindex="-1">
                        <i class="fa fa-angle-left"></i>
                    </a>
                </li>
                <li>
                    <a class="flex-next" href="#" tabindex="-1">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>	