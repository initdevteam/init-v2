<?php
/**
 * @file views-isotope-filter-block.tpl.php
 * Default simple view template to display a list of rows.
 */
?>
<div class="f-width">
<div id="isotope-options">
<div class="filter-container cf">
			<span>Filter by:</span>
  <ul id="filters" class="filters cf option-set clearfix" data-option-key="filter">
  <li><a href="#filter" data-option-value="*" class="filterbutton"><?php print t('All'); ?></a></li>
    <?php foreach ( $rows as $id => $row ): ?>
      
      <?php 
      // remove characters that cause problems with classes
      // this is also do to the isotope elements
      $dataoption = trim(strip_tags(strtolower($row)));
      $dataoption = str_replace(' ', '-', $dataoption);
      $dataoption = str_replace('/', '-', $dataoption);
      $dataoption = str_replace('&amp;', '', $dataoption); 
      ?>
          
      <li><a class="filterbutton" data-option-value=".<?php print $dataoption; ?>" href="#filter"><?php print trim($row); ?></a></li>

      
    <?php endforeach; ?>
    
  </ul>  
</div>
</div>
</div>