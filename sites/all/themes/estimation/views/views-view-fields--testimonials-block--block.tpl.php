<li>
    <div class="testimony boxshadow cf">
        <div class="testimony-inner">
            <img class="portrait" src="<?php echo $fields['field_photo']->content; ?>" alt="image description">
            <blockquote class="testimony-text">
                <?php
                    echo $fields['body']->content;
                ?>									
            </blockquote>	

            <div class="testimony-author">
                <h4><?php echo $fields['title']->content; ?></h4>
                <span><?php echo $fields['field_description']->content; ?></span>						
            </div><!-- end testimony-author -->
        </div><!-- end testimony -->
    </div>
</li>
