<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="animated " data-animation="fadeInLeft">	
    <div class="col-row">
        <div class="jcarousel-wrapper">
            <div class="jcarousel promotionalSlider">
                <ul>
                    <?php foreach ($rows as $id => $row): ?>
                        <li><?php print $row; ?></li>
                    <?php endforeach; ?>
                </ul>
                <div class="clearfix"></div>
                <?php if(count($rows) > 1): ?>
                <p class="jcarousel-pagination"></p>
                <?php endif; ?>
            </div>
            
        </div>
    </div>
</div>
<?php if(count($rows) > 1): ?>
    <script>
        jQuery(document).ready(function(){
            var jcarousel = jQuery('.jcarousel.promotionalSlider');

            jcarousel.on('jcarousel:reload jcarousel:create', function () {
                var carousel = jQuery(this),
                width = carousel.innerWidth();

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            }).jcarousel({
                wrap: 'circular'
            }).jcarouselSwipe({
                draggable: false
            });
            jcarousel.jcarouselAutoscroll({
                autostart: true,
                interval: 6000,
                target: '+=1'
            });
            jcarousel.hover(function() {
                jQuery(this).jcarouselAutoscroll('stop');
            }, function() {
                jQuery(this).jcarouselAutoscroll('start');
            });
            
            jQuery('.jcarousel.promotionalSlider > .jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
                jQuery(this).addClass('active');
            }).on('jcarouselpagination:inactive', 'a', function() {
                jQuery(this).removeClass('active');
            }).on('click', function(e) {
                e.preventDefault();
            }).jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">&nbsp;</a>';
                }
            });
        });
    </script>
<?php endif; ?>