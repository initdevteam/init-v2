<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<br><br>
<section class="content boxshadow" id="team">
	<div class="container-center">
	
		<div class="section-title align-center">
			<h2>Meet The Team</h2>
			<h3 class="sub-heading" style="text-transform: none;">INIT was born from the wishes of its founders and managers, but the real power is in the team that follows them.</h3>
		</div>
        <div class="animated meetTeamBlock" data-animation="fadeInRight">	
            <div class="col-row">
                <?php foreach ($rows as $id => $row): ?>
                    <?php print $row; ?>
                <?php endforeach; ?>
            </div>
        </div>	
    </div>	
</section>	