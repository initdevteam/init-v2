<div class="one-half">
    <div class="testimony boxshadow cf">
        <div class="testimony-inner">
            <?php echo $fields['picture']->content; ?>
            <div class="infoBlock">
                <div class="clientBlock">
                    <div class="testimony-author">
                        <h4><?php echo $fields['field_full_name']->content; ?></h4>
                        <span><?php echo $fields['field_position']->content; ?></span>						
                    </div>
                    <blockquote class="testimony-text">
                        <?php
                            echo $fields['field_philosophy']->content;
                        ?>									
                    </blockquote>
                    <div class="text-right text-center-md mt10">
                        <a class="button small-btn showBiography" href>Show Biography</a>
                    </div>
                </div>
                <div class="biographyBlock">
                    <div class="testimony-author text-right">
                        <a class="button small-btn showBiography" href>Hide Biography</a>
                    </div>
                    <?php
                        echo $fields['field_biography']->content;
                    ?>
                </div>
            </div>
            <!-- end testimony-author -->
        </div><!-- end testimony -->
    </div>
</div>
