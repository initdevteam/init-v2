<?php
if (theme_get_setting('estimation_preloader') == TRUE) {
    print '<!-- Preloader -->
<div id="preloader">
    <div id="status"></div>
</div>';
  }
  else {

  }
?> 

<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '<div class="wrapper">';
  }
  else {

  }
?> 

<header id="header">
	<div class="container-center cf">
      <div class="logo">
        <?php if($logo): ?>
          <a href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"></a>
        <?php else: ?>
          <h1 class="site-name"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
           <p class="tagline"><?php print $site_slogan; ?></p>
        <?php endif; ?>
       
      </div>
		<div class="userInfoBlock">
            <a href="#" class="userOpen openBlock"><i class="fa fa-user" aria-hidden="true"></i></a>
            <?php if ($page['user-login']): ?>
                <div class="userForm">
                    <?php print render($page['user-login']); ?>
                </div> <!-- /.section, /#sidebar-first -->
            <?php else: ?>
                <?php if (!empty($_SESSION['client'])): ?>
                    <div class="userForm">
                        <p class="logoutBlock"><a href="/logout" ><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Logout</a></p>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
		<div class="search-container">
			<a id="toggle-search" class="search-button search-open" href="#"><i class="fa fa-search"></i></a>
			<div class="search-panel cf">	
                <?php $block = module_invoke('search', 'block_view', 'search'); print render($block); ?>	  
			</div><!-- end search-panel -->
		</div><!-- search-container -->
		<nav id="main-navigation" class="navigation cf">
            <?php print render($page['main_menu']); ?>
		</nav>
	</div><!-- end container-center -->
</header>

    <!-- #page-title start -->
    <?php 
    
    $bg_image = base_path() . path_to_theme() . '/img/web360/cover.jpg';
        $cover_style = 'background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, rgba(0, 0, 0, .2)), color-stop(100%, rgba(0, 0, 0, .9))), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -webkit-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -moz-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -o-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background-size: cover;';
    ?>
    <section class="page-title section-bg-1 article-cover web360" style="<?php print $cover_style; ?>">
        <div class="page-title-inner container-center cf" >
            <h1 class="container-center">Renová tu sitio web!</h1>
            <h4 class="article-subtitle">Sitios adaptativos, elegantes y atractivos, que generan confianza e impacto en tus clientes.</h4>
        </div><!-- end container-center -->
    </section>
    
<?php if ($page['content_top']): ?>
    <section class="content-top">	
        <?php print render($page['content_top']); ?>
    </section>
<?php endif; ?>
    
<?php if ($page['slider']): ?>
    <section class="container-slider">	
        <div id="slider">	
            <?php print render($page['slider']); ?>
        </div><!-- end Slider -->   
    </section>
<?php endif; ?>

<?php if ($page['featured']): ?>
    <section class="content">
        <div class="container-center">
            <?php print render($page['featured']); ?>

        </div><!-- end container-center -->
    </section><!-- end content -->
<?php endif; ?> 
<?php if ($page['content_no_wrap']): ?>   
    <section class="color-box-holder box-4 section-bg-1 services-section">
        <div class="arrow-white-down" style="margin-top: -52px;"></div>
        <?php print render($page['content_no_wrap']); ?>
    </section>
<?php endif; ?> 
     
     


<section class="content container-center cf <?php if (drupal_is_front_page()): ?>
<?php
if (theme_get_setting('estimation_bg') == TRUE) {
    print 'section-bg-4';
  }
  else {

  }
?>
<?php endif; ?>
"> 

      <?php if ($page['sidebar']): ?>
        <div class="main-content-left">
      <?php endif; ?>
      <?php if (!$page['sidebar']): ?>
        <div class="no-sidebar">
      <?php endif; ?>

        <?php if ($messages): ?>
            <div id="messages">
                <?php print $messages; ?>
                <span class="btnHide">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </span>
            </div>
        <?php endif; ?>
         <?php if ($title): ?><h1 class="title" id="pt"><?php print $title; ?></h1><?php endif; ?>
                
        <?php if ($tabs): ?><div class="d-tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <section class="container-center">

            <div class="col-row">
                    <div class="full-width align-center">
                        <br>
                        <h2 class="heading-extra-big">Diseños basados en plantillas</h2>
                        <br>
                            <div class="medium-message">	
                                    <p>Sitios creados a partir de plantillas elegantes y atractivas, que permiten reducir tiempo y costo sin perder calidad e impacto.</p>
                            </div><!-- end big-message -->	
                            <br>
                    </div><!-- end full-width -->
            </div>
            <div class="col-row">				

                <div class="one-third">
                    <div class="feature-box animated" data-animation-start="onLoad" data-animation="fadeInUp">
                        <div class="feature-box-title cf">
                            <div class="feature-box-icon bg-yellow">
                                <i class="fa fa-tablet"></i>
                            </div>
                            <h4>Diseños adaptativos</h4>
                        </div><!-- end feature-box-title -->

                        <p>Tu sitio se verá excelente en celulares, tablets, notebooks, computadoras o televisores.</p>

                    </div>	

                    <div class="feature-box animated" data-animation="fadeInUp" data-animation-start="onLoad" data-animation-delay="500">
                        <div class="feature-box-title cf">
                            <div class="m360-icon feature-box-icon bg-orange">
                                <p style="font-size: 10px;line-height: 11px;margin-top: 11px;">MIRO<br><strong style="color: #fff; font-size:14px; font-weight: bold;">360</strong></p>
                            </div>
                            <h4>Conexión con <img style="vertical-align: middle;" src="/sites/all/themes/estimation/img/web360/miro-logo-small.png" /></h4>
                        </div><!-- end feature-box-title -->

                        <p>Tu sitio conectado directamente con MIRO360, el portal creado por Cadeiros y CEIR que ya se posiciona como el más completo de Rosario y la Región.</p>

                    </div>	
                </div>

                <div class="one-third img-lg">
                    <img class="animated" data-animation="fadeInRight" src="<?php print base_path() . path_to_theme() ?>/img/web360/responsive.png" alt="Responsive Design" />
                    <div class="align-center"><a href="#contact-chat" class="button large-btn"><i class="fa  fa-comments"></i> Contactanos</a></div>
                </div><!-- end  -->

                <div class="one-third">	
                    <div class="feature-box animated on-start" data-animation="fadeInUp" data-animation-start="onLoad" data-animation-delay="900">
                        <div class="feature-box-title cf">
                            <div class="feature-box-icon bg-red">
                                <i class="fa fa-building-o"></i>
                            </div>
                            <h4>Gestioná tus propiedades</h4>
                        </div><!-- end feature-box-title -->

                        <p>Gestioná tus propiedades directamente desde tu sitio web y las mismas se publicarán directamente en MIRO360.</p>

                    </div>		
                    <div class="feature-box animated on-start" data-animation="fadeInUp" data-animation-start="onLoad" data-animation-delay="900">
                        <div class="feature-box-title cf">
                            <div class="feature-box-icon bg-gray">
                                <i class="fa fa-search"></i>
                            </div>
                            <h4>Tu propio buscador</h4>
                        </div><!-- end feature-box-title -->

                        <p>Podrás contar con tu propio buscador de propiedades, brindandole a tus usuarios un nuevo canal para ver la oferta disponible.</p>

                    </div>		
                </div><!-- end  -->
                <div class="one-third img-sm">
                    <img class="animated" data-animation="fadeInRight" src="<?php print base_path() . path_to_theme() ?>/img/web360/responsive.png" alt="Responsive Design" />
                    <div class="align-center"><a href="#contact-chat" class="button large-btn"><i class="fa  fa-comments"></i> Contactanos</a></div>
                </div><!-- end  -->


            </div><!-- end colr-row -->
        </section><!-- end content -->	

        <div class="separator-1"></div><!-- end separator -->

        <section class="content">					

            <div class="col-row">
                <div class="one-third">
                    <img class="animated" data-animation="fadeInRight" src="<?php print base_path() . path_to_theme() ?>/img/web360/customdesign.png" alt="Custom Design" />
                </div>
                <div class="two-third">
                    <div class="animated"  data-animation="fadeInLeft">
                        <h2  class="heading-extra-big">NECESITAS UN DISEÑO A MEDIDA?</h2><br>
                        <div class="medium-message" style="text-align: left;">
                            <p>Nuestro equipo de diseño y desarrollo te puede asesorar.<br><br>Contactanos sin compromiso y te lo cotizamos</p>
                        </div>
                        <p><a href="#contact-chat" class="button large-btn"><i class="fa  fa-comments"></i> Contactanos</a></p>
                    </div>									
                </div><!-- end  -->

            </div><!-- end col-row -->
        </section><!-- end content -->	

        <div class="separator-1"></div><!-- end separator -->

        <section class="content">					

            <div class="section-title align-center">
                <h2  class="heading-extra-big">¿YA TENES UNA WEB QUE TE ENCANTA?</h2>
            </div>			

            <div class="col-row">
                
                <div class="one-third">
                    <div class="animated"  data-animation="fadeInLeft">
                        <h3>¿Tu web ya cuenta con gestión de propiedades?</h3>
                        <p>Modificamos tu sitio web para que tus propiedades se publiquen automáticamente en <img style="vertical-align: middle;" src="/sites/all/themes/estimation/img/web360/miro-logo-small.png" /></p>
                    </div>
                </div>
                <div class="one-third align-center img-lg">
                    <img class="animated" data-animation="fadeInRight" src="<?php print base_path() . path_to_theme() ?>/img/web360/ownweb.png" alt="Own Web" />
                    <br>
                    <br>
                    <a href="#contact-chat" class="button large-btn"><i class="fa fa-comments"></i> Contactanos</a>
                </div>
                <div class="one-third">
                    <div class="animated"  data-animation="fadeInLeft">
                        <h3>¿Aún no tienes tus propiedades en tu sitio web?</h3>
                        <p>Añadimos la posibilidad de crear y publicar propiedades para que tus usuarios puedan buscarlas dentro de tu sitio web.</p>
                        <p>Además, las propiedades que cargues en tu sitio se publicarán automáticamente en <img style="vertical-align: middle;" src="/sites/all/themes/estimation/img/web360/miro-logo-small.png" /></p>
                    </div>
                </div>
                <div class="one-third align-center img-sm">
                    <img class="animated" data-animation="fadeInRight" src="<?php print base_path() . path_to_theme() ?>/img/web360/ownweb.png" alt="Own Web" />
                    <br>
                    <br>
                    <a href="#contact-chat" class="button large-btn"><i class="fa fa-comments"></i> Contactanos</a>
                </div>
            </div><!-- end col-row -->
        </section><!-- end content -->	
        
</div><!-- end main-content -->

<?php if ($page['sidebar']): ?>
    <!-- sidebar first -->
    <div class="sidebar sb-right">
        <div class="sidebar-box">
            <?php print render($page['sidebar']); ?>
        </div>
    </div>
    <!-- // sidebar -->
<?php endif; ?>
</section><!-- end -->
   
<section class="color-box-holder box-4 section-bg-1 services-section web360">
    <div class="arrow-white-down" style="margin-top: -52px;"></div>
    <div class="region region-content-no-wrap">
        <div id="block-block-26" class="block block-block contextual-links-region align-center">
            
            <h2 class="heading-extra-big text-white">¿Por qué confiar en INIT Consultants?</h2>
            <br>
            <br>

            <div class="content">
                <div class="container-center" id="services">
                    <div class="col-row">
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa  fa-check-square-o"></i>
                                            <h3>Experiencia</h3>
                                            <h4 class="subtitle"><span>En sistemas de Real State</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">Experiencia en Real State</p>
                                            <p>Diversos productos para empresas del rubro inmobiliario y de la construcción dan cuenta de nuestra calidad, entre los que se destaca MIRO360</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa fa-handshake-o"></i>
                                            <h3>Visión Estratégica</h3>
                                            <h4 class="subtitle"><span>Para Superar objetivos</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">Visión Estratégica</p>
                                            <p>No solo diseñamos su sitio web, lo ayudamos a armar un negocio en torno a el haciendolo rentable.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa fa-map-marker"></i>
                                            <h3>Somos de Rosario</h3>
                                            <h4 class="subtitle"><span>Y llegamos al mundo</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">Estamos Cerca Tuyo</p>
                                            <p>Aunque hoy estamos exportando servicios a más de 12 paises, nuestra base está en Rosario, lo cual nos permite vivir y entender el mercado Rosarino</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa  fa-thumbs-o-up"></i>
                                            <h3>Estamos Avalados</h3>
                                            <h4 class="subtitle"><span>Por las cámaras inmobiliarias</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">CEIR y Cadeiros nos avalan</p>
                                            <p>Nuestro excelente trabajo en el desarrollo del portal MIRO360 hace que tanto Cadeiros como CEIR nos depositen su confianza como proveedores de desarrollo web.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</section>

<section class="content light-section section-bg-1 web360 recomendacion">
    <div class="align-center">
        <h2 class="heading-extra-big">Nos Recomiendan</h2>
        <h3 class="sub-heading">Las Cámaras Inmobiliarias de Rosario</h3>
    </div>
    <br>
    <div class="container-center">
        <div class="col-row">
            <div class="one-half aval-cadeiros box-shadow">
                <img class="animated" data-animation="fadeInLeft" src="<?php print base_path() . path_to_theme() ?>/img/web360/cadeiros.png" alt="Cadeiros" />
            </div>
            <div class="one-half aval-ceir box-shadow">
                <img class="animated" data-animation="fadeInRight" src="<?php print base_path() . path_to_theme() ?>/img/web360/ceir.png" alt="CEIR" />
            </div>
        </div>
    </div>
    <div class="align-center">
        <div class="medium-message">	
            <p>Consultanos por descuentos exclusivos para miembros de Cadeiros y CEIR</p>
            <a href="#contact-chat" class="button large-btn"><i class="fa fa-comments"></i> Contactanos</a>
        </div>
    </div>
</section>


<?php if ($page['after_bottom']): ?>	
    <section class="twitter-holder section-bg-7">
        <?php print render($page['after_bottom']); ?>	
    </section>
<?php endif; ?>

<?php if ($page['after_content']): ?>
    <section class="content">
        <div class="after-content">		
            <?php print render($page['after_content']); ?>
        </div>
    </section><!-- end -->
<?php endif; ?>

<?php if ($page['content_bottom']): ?>
    <section class="content light-section section-bg-1">
        <div class="container-center">		
            <div class="col-row">
                <?php print render($page['content_bottom']); ?>
            </div><!-- end col-row -->		
        </div><!-- end container-center -->
    </section><!-- end content -->
<?php endif; ?> 
    
	
<?php if($page['postscript_first'] || $page['postscript_second'] || $page['postscript_third'] || $page['postscript_fourth'] ) : ?>
    <section class="content">
        <div class="container-center">
            <?php if ($page['postscript_first']): ?>
                <?php print render($page['postscript_first']); ?>
            <?php endif; ?>
            <?php if ($page['postscript_second']): ?>
                <?php print render($page['postscript_second']); ?>
            <?php endif; ?>

            <div class="col-row">
                <div class="one-half">
                    <?php if ($page['postscript_fourth']): ?>
                        <?php print render($page['postscript_fourth']); ?>
                    <?php endif; ?>
                </div>
            </div>
            <!-- end col-row -->		
        </div><!-- end container-center -->
    </section><!-- end content -->	
<?php endif; ?>
<?php if ($page['postscript_third']): ?> 
    <div class="postscript_third">	
        <?php print render($page['postscript_third']); ?>
    </div>	
<?php endif; ?>
<section class="content">
    <?php if ($page['bottom']): ?>
        <div class="container-center">
            <?php print render($page['bottom']); ?>	
        </div>
    <?php endif; ?>
</section>


<footer id="footer" class="content">
	<div class="footer-main container-center">
		<div class="col-row">
            <?php if ($page['f1'] || $page['f2'] || $page['f3'] || $page['f4']) : ?>
                <?php if ($page['f1']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f1']); ?>
                    </div><!-- end  -->
                <?php endif; ?> 
                <?php if ($page['f2']): ?>
                    <div class="one-fourth cf">
                        <?php print render($page['f2']); ?>
                    </div><!-- end  -->	
                <?php endif; ?> 

                <?php if ($page['f3']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f3']); ?>						
                    </div><!-- end  -->			
                <?php endif; ?> 

                <?php if ($page['f4']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f4']); ?>
                    </div><!-- end  -->					
                <?php endif; ?> 
            <?php endif; ?> 
		</div><!-- end col-row -->	
       <?php if($page['f5'] || $page['f6'] || $page['f7'] || $page['f8']) : ?>
		<div class="separator-1"></div><!-- end separator -->

		<div class="col-row">			
          <?php if ($page['f5']): ?>
			<div class="one-fourth">
				 <?php print render($page['f5']); ?>	
			</div>
         <?php endif; ?> 
          <?php if ($page['f6']): ?>
			<div class="one-fourth">
				 <?php print render($page['f6']); ?>
			</div>		
          <?php endif; ?> 
          <?php if ($page['f7']): ?>
			<div class="one-fourth">
            <?php print render($page['f7']); ?>
			</div>		
            <?php endif; ?>
            <?php if ($page['f8']): ?> 
			<div class="one-fourth cf">
			<?php print render($page['f8']); ?>
			</div>									
				<?php endif; ?> 
		</div><!-- end col-row -->
<?php endif; ?> 
	</div><!-- end footer-main -->

	<div class="footer-bottom container-center cf">					
			
		<div class="bottom-left">	
          <?php if ($page['footer-a']): ?>
          <?php print render($page['footer-a']); ?>
          <?php endif; ?>  								
							
		</div><!-- end bottom-left -->		
		
		<div class="bottom-right">
         <?php if ($page['footer-b']): ?>
         <?php print render($page['footer-b']); ?>
         <?php endif; ?>
			
		</div><!-- end bottom-right -->				
			
	</div><!-- end footer-bottom -->
</footer>
<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '</div><!-- end wrapper -->';
  }
  else {

  }
?> 
<div class="scroll-top"><a href="#"><i class="fa fa-angle-up"></i></a></div>