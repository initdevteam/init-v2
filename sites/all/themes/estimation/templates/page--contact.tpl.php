<?php
if (theme_get_setting('estimation_preloader') == TRUE) {
    print '<!-- Preloader -->
<div id="preloader">
    <div id="status"></div>
</div>';
  }
  else {

  }
?> 

<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '<div class="wrapper">';
  }
  else {

  }
?> 

<header id="header">
	<div class="container-center cf">
      <div class="logo">
        <?php if($logo): ?>
          <a href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"></a>
        <?php else: ?>
          <h1 class="site-name"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
           <p class="tagline"><?php print $site_slogan; ?></p>
        <?php endif; ?>
       
      </div>
			
		<div class="search-container">
			<a id="toggle-search" class="search-button search-open" href="#"><i class="fa fa-search"></i></a>
			<div class="search-panel cf">	
            <?php $block = module_invoke('search', 'block_view', 'search'); print render($block); ?>	  
			 
			</div><!-- end search-panel -->
		</div><!-- search-container -->

		<nav id="main-navigation" class="navigation cf">
         <?php print render($page['main_menu']); ?>
			
		</nav>
	</div><!-- end container-center -->
</header>

<?php if ($page['content_top']): ?>
<section class="content-top">	

 <?php print render($page['content_top']); ?>

</section>
<?php endif; ?>
 <?php if ($page['slider']): ?>
<section class="container-slider">	
	<div id="slider">	
 <?php print render($page['slider']); ?>
	</div><!-- end Slider -->   
</section>
<?php endif; ?>

 <?php if ($page['featured']): ?>
<section class="content">
	<div class="container-center">
       <?php print render($page['featured']); ?>
		
	</div><!-- end container-center -->
</section><!-- end content -->
     <?php endif; ?> 
  <?php if ($page['content_no_wrap']): ?>   
     <section class="color-box-holder box-4 section-bg-1">
      <?php print render($page['content_no_wrap']); ?>
      </section>
      <?php endif; ?> 
     
     

<section class="content">
	<div class="container-center">	
		<div class="col-row">

<div class="one-half">

	 <?php if ($messages): ?>
                
                <div id="messages">
                <?php print $messages; ?>
                </div>
             
                <?php endif; ?>
        <!--  <?php if ($title): ?><h1 class="title" id="pt"><?php print $title; ?></h1><?php endif; ?> -->
                <h2 class="heading-big">Drop Us a Line</h2>
        <?php if ($tabs): ?><div class="d-tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php print render($page['content']); ?>
		

</div><!-- end main-content -->

<?php if ($page['sidebar']): ?>
        <!-- sidebar first -->
        
        <div class="one-half">
        <div class="sidebar-box">
          <?php print render($page['sidebar']); ?>
        </div>
        </div>

        <!-- // sidebar -->
<?php endif; ?>
</div></div>
</section><!-- end -->
<?php if ($page['after_content']): ?>
<section class="content">
<div class="after-content">		
<?php print render($page['after_content']); ?>
</div>
</section><!-- end -->
<?php endif; ?>
	
   <?php if ($page['content_bottom']): ?>
<section class="content light-section section-bg-1">
	<div class="container-center">		
		<div class="col-row">

 <?php print render($page['content_bottom']); ?>

		</div><!-- end col-row -->		
	</div><!-- end container-center -->
</section><!-- end content -->
<?php endif; ?> 
    
	
<?php if($page['postscript_first'] || $page['postscript_second'] || $page['postscript_third'] || $page['postscript_fourth'] ) : ?>
<section class="content">
	<div class="container-center">
	        <?php if ($page['postscript_first']): ?>
			<?php print render($page['postscript_first']); ?>
			<?php endif; ?>
		
		
		<div class="col-row">
        <?php if ($page['postscript_second']): ?>
        <div class="one-half animated" data-animation="fadeInLeft">	
		<?php print render($page['postscript_second']); ?>
         </div>	
		<?php endif; ?>		
			
			
				
				
			<?php if ($page['postscript_third']): ?> 
			<div class="one-half animated" data-animation="fadeInRight">	
			<?php print render($page['postscript_third']); ?>
            </div>	
			<?php endif; ?>

			<?php if ($page['postscript_fourth']): ?>

            <?php print render($page['postscript_fourth']); ?>

            <?php endif; ?>
				
			
		</div><!-- end col-row -->		
		
	</div><!-- end container-center -->
</section><!-- end content -->	
	<?php endif; ?>
<section class="content">
<?php if ($page['bottom']): ?>
<div class="container-center">
<?php print render($page['bottom']); ?>	
</div>
<?php endif; ?>
</section>
<?php if ($page['after_bottom']): ?>	
<section class="twitter-holder section-bg-1">
<?php print render($page['after_bottom']); ?>	
</section>
<?php endif; ?>
<footer id="footer" class="content">
	<div class="footer-main container-center">
		<div class="col-row">
            <?php if($page['f1'] || $page['f2'] || $page['f3'] || $page['f4']) : ?>
            <?php if ($page['f1']): ?>
            <div class="one-fourth">
			<?php print render($page['f1']); ?>
            </div><!-- end  -->
            <?php endif; ?> 
            <?php if ($page['f2']): ?>
			<div class="one-fourth cf">
            <?php print render($page['f2']); ?>
            </div><!-- end  -->	
			<?php endif; ?> 
					
			<?php if ($page['f3']): ?>
			<div class="one-fourth">
             <?php print render($page['f3']); ?>						
			</div><!-- end  -->			
			<?php endif; ?> 
            
            <?php if ($page['f4']): ?>
			<div class="one-fourth">
            <?php print render($page['f4']); ?>
			</div><!-- end  -->					
			<?php endif; ?> 
            <?php endif; ?> 
		</div><!-- end col-row -->	
       <?php if($page['f5'] || $page['f6'] || $page['f7'] || $page['f8']) : ?>
		<div class="separator-1"></div><!-- end separator -->

		<div class="col-row">			
          <?php if ($page['f5']): ?>
			<div class="one-fourth">
				 <?php print render($page['f5']); ?>	
			</div>
         <?php endif; ?> 
          <?php if ($page['f6']): ?>
			<div class="one-fourth">
				 <?php print render($page['f6']); ?>
			</div>		
          <?php endif; ?> 
          <?php if ($page['f7']): ?>
			<div class="one-fourth">
            <?php print render($page['f7']); ?>
			</div>		
            <?php endif; ?>
            <?php if ($page['f8']): ?> 
			<div class="one-fourth cf">
			<?php print render($page['f8']); ?>
			</div>									
				<?php endif; ?> 
		</div><!-- end col-row -->
<?php endif; ?> 
	</div><!-- end footer-main -->

	<div class="footer-bottom container-center cf">					
			
		<div class="bottom-left">	
          <?php if ($page['footer-a']): ?>
          <?php print render($page['footer-a']); ?>
          <?php endif; ?>  								
							
		</div><!-- end bottom-left -->		
		
		<div class="bottom-right">
         <?php if ($page['footer-b']): ?>
         <?php print render($page['footer-b']); ?>
         <?php endif; ?>
			
		</div><!-- end bottom-right -->				
			
	</div><!-- end footer-bottom -->
</footer>
<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '</div><!-- end wrapper -->';
  }
  else {

  }
?> 
<div class="scroll-top"><a href="#"><i class="fa fa-angle-up"></i></a></div>