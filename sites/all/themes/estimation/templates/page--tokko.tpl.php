<?php
if (theme_get_setting('estimation_preloader') == TRUE) {
    print '<!-- Preloader -->
<div id="preloader">
    <div id="status"></div>
</div>';
  }
  else {

  }
?> 

<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '<div class="wrapper">';
  }
  else {

  }
?> 

<header id="header">
	<div class="container-center cf">
      <div class="logo">
        <?php if($logo): ?>
          <a href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"></a>
        <?php else: ?>
          <h1 class="site-name"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
           <p class="tagline"><?php print $site_slogan; ?></p>
        <?php endif; ?>
       
      </div>
		<div class="userInfoBlock">
            <a href="#" class="userOpen openBlock"><i class="fa fa-user" aria-hidden="true"></i></a>
            <?php if ($page['user-login']): ?>
                <div class="userForm">
                    <?php print render($page['user-login']); ?>
                </div> <!-- /.section, /#sidebar-first -->
            <?php else: ?>
                <?php if (!empty($_SESSION['client'])): ?>
                    <div class="userForm">
                        <p class="logoutBlock"><a href="/logout" ><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Logout</a></p>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
		<div class="search-container">
			<a id="toggle-search" class="search-button search-open" href="#"><i class="fa fa-search"></i></a>
			<div class="search-panel cf">	
                <?php $block = module_invoke('search', 'block_view', 'search'); print render($block); ?>	  
			</div><!-- end search-panel -->
		</div><!-- search-container -->
		<nav id="main-navigation" class="navigation cf">
            <?php print render($page['main_menu']); ?>
		</nav>
	</div><!-- end container-center -->
</header>

    <!-- #page-title start -->
    <?php 
    
    $bg_image = base_path() . path_to_theme() . '/img/tokko/desktop.jpg';
        $cover_style = 'background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, rgba(0, 0, 0, .2)), color-stop(100%, rgba(0, 0, 0, .9))), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -webkit-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -moz-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -o-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background-size: cover;';
    ?>
    <section class="page-title section-bg-1 article-cover web360" style="<?php print $cover_style; ?>">
        <div class="page-title-inner container-center text-center cf" >
            <h1 class="container-center">Conectá tu web con <img style="vertical-align: middle;" src="/sites/all/themes/estimation/img/tokko/logotokko-medium-shadow.png"/></h1>
            <h4 class="article-subtitle">Aumentá la productividad de tu sitio web conectandoló al CRM Tokko Broker.</h4>
        </div><!-- end container-center -->
    </section>
    
<?php if ($page['content_top']): ?>
    <section class="content-top">	
        <?php print render($page['content_top']); ?>
    </section>
<?php endif; ?>
    
<?php if ($page['slider']): ?>
    <section class="container-slider">	
        <div id="slider">	
            <?php print render($page['slider']); ?>
        </div><!-- end Slider -->   
    </section>
<?php endif; ?>

<?php if ($page['featured']): ?>
    <section class="content">
        <div class="container-center">
            <?php print render($page['featured']); ?>

        </div><!-- end container-center -->
    </section><!-- end content -->
<?php endif; ?> 
<?php if ($page['content_no_wrap']): ?>   
    <section class="color-box-holder box-4 section-bg-1 services-section">
        <div class="arrow-white-down" style="margin-top: -52px;"></div>
        <?php print render($page['content_no_wrap']); ?>
    </section>
<?php endif; ?> 
     
     


<section class="content container-center cf <?php if (drupal_is_front_page()): ?>
<?php
if (theme_get_setting('estimation_bg') == TRUE) {
    print 'section-bg-4';
  }
  else {

  }
?>
<?php endif; ?>
"> 

      <?php if ($page['sidebar']): ?>
        <div class="main-content-left">
      <?php endif; ?>
      <?php if (!$page['sidebar']): ?>
        <div class="no-sidebar">
      <?php endif; ?>

        <?php if ($messages): ?>
            <div id="messages">
                <?php print $messages; ?>
                <span class="btnHide">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </span>
            </div>
        <?php endif; ?>
         <?php if ($title): ?><h1 class="title" id="pt"><?php print $title; ?></h1><?php endif; ?>
                
        <?php if ($tabs): ?><div class="d-tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <section class="container-center tokko-init-services">

            <section class="container-center">
	
                <div class="section-title align-center">
                        <h2>¿Como podemos ayudarte?</h2>
                </div>		
                <div class="col-row">
                        <div class="one-fourth">
                                <div class="step">
                                        <div class="step-icon bg-yellow">
                                                <i class="fa fa-object-group"></i>
                                        </div><!-- end step-icon -->
                                        <h4 class="heading-big">Diseño web a medida</h4>
                                        <p>Creamos sitios web a medida de cada cliente superando ampliamente sus expectativas.</p>
                                </div><!-- end step -->
                        </div>

                        <div class="one-fourth">
                                <div class="step">
                                        <div class="step-icon bg-orange">
                                                <i class="fa fa-window-restore"></i>
                                        </div><!-- end step-icon -->
                                        <h4 class="heading-big">Diseño web basado en plantillas</h4>
                                        <p>Creamos sitios webs atractivos y dinámicos basados en plantillas que permiten reducir costos y tiempos.</p>
                                </div><!-- end step -->
                        </div>

                        <div class="one-fourth">
                                <div class="step">
                                        <div class="step-icon bg-red">
                                                <img src="/sites/all/themes/estimation/img/tokko/tokko-icon.png" />
                                        </div><!-- end step-icon -->
                                        <h4 class="heading-big">Conexión con Tokko Broker</h4>
                                        <p>Conectamos tu sitio web con la api de Tokko Broker para que sea más productivo.</p>
                                </div><!-- end step -->
                        </div>

                        <div class="one-fourth">
                                <div class="step">
                                        <div class="step-icon bg-gray">
                                                <i class="fa fa-paint-brush"></i>
                                        </div><!-- end step-icon -->
                                        <h4 class="heading-big">Rediseño de tu sitio web</h4>
                                        <p>Rediseñamos tu sitio web para que se vea moderno y funcional siguiendo los mejores patrones de diseño.</p>
                                </div><!-- end step -->
                        </div>
                </div><!-- end col-row -->
            </section><!-- end  -->
            
            <div class="align-center"><a href="#contact-chat" class="button large-btn"><i class="fa  fa-comments"></i> Contactanos</a></div>            
</div><!-- end main-content -->
</section><!-- end -->

<div class="separator-1"></div><!-- end separator -->

<section class="content" style="margin-top: 50px;">
    <div class="container-center">
            <div class="section-title align-center">
                <h2>Ventajas de utilizar <img width="100px" style="vertical-align: middle;" src="/sites/all/themes/estimation/img/tokko/logotokko.svg"/></h2>
            </div>

            <div class="col-row tokko-benefits">
                    <div class="one-third">
                            <div class="color-box bg-red">
                                <i class="fa fa-crosshairs"></i>
                                <div class="color-box-title tilted">
                                    <h4 class="subtitle"><span>OPTIMICE LA GESTIÓN DE SUS CLIENTES CON EL</span></h4>
                                    <h3>MODULO DE OPORTUNIDADES</h3>
                                </div>
                                <p>Los vendedores de su inmobiliaria tendrán una herramienta que los guiará en el seguimiento de cada oportunidad comercial que tengan con sus clientes.
Los ayudará a administrar los tiempos máximos para atender a los interesados en cada etapa de su consulta.
</p>
<p>Un seguimiento eficaz aumenta las ventas y mejora la atención al cliente.</p>
                            </div><!-- end box -->	
                    </div>

                    <div class="one-third">	
                            <div class="color-box bg-red">
                                <i class="fa fa-home"></i>
                                <div class="color-box-title tilted">
                                    <h4 class="subtitle"><span>AMPLIE SU CARTERA DE INMUEBLES A TRAVÉS DE LA</span></h4>
                                    <h3>RED INTERINMOBILIARIA</h3>
                                </div>
                                <p>Además de ofrecer a sus clientes su propia cartera de inmuebles, con la red Tokko Broker podrá también consultar productos de otras inmobiliarias aumentando la posibilidad de encontrar el inmueble buscado y brindar un mejor servicio.</p>
                                <p>La red también le permitirá formar distintos grupos cerrados con inmobiliarias colegas.</p>
                            </div><!-- end box -->		
                    </div>

                    <div class="one-third">	
                            <div class="color-box bg-red tokko-benefits-last">
                                <i class="fa fa-share-alt"></i>
                                <div class="color-box-title tilted">
                                    <h4 class="subtitle"><span>REPUBLIQUE EN LOS MEJORES PORTALES Y GENERE</span></h4>
                                    <h3>SEGUIMIENTOS AUTOMATICOS</h3>
                                </div>
                                <p>Simplifique su trabajo cargando y gestionando sus propiedades en Tokko Broker manteniendo así toda la información actualizada desde un mismo lugar.
Difunda en los mejores portales inmobiliarios y en su página web.</p>
                                <p>Al recibir nuevas consultas, los interesados ingresarán directamente al software con un seguimiento automático activo que diariamente les enviará propiedades similares a las de su búsqueda.</p>
                            </div><!-- end box -->	
                    </div>
            </div><!-- end col-row -->
    </div><!-- end container-center -->
</section><!-- end content -->
 
<section class="color-box-holder box-4 section-bg-1 services-section web360">
    <div class="arrow-white-down" style="margin-top: -52px;"></div>
    <div class="region region-content-no-wrap">
        <div id="block-block-26" class="block block-block contextual-links-region align-center">
            
            <h2 class="heading-extra-big text-white">¿Por qué confiar en INIT Consultants?</h2>
            <br>
            <br>

            <div class="content">
                <div class="container-center" id="services">
                    <div class="col-row">
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa  fa-check-square-o"></i>
                                            <h3>Experiencia</h3>
                                            <h4 class="subtitle"><span>En desarrollos de Real State</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">Experiencia en Real State</p>
                                            <p>Diversos productos para empresas del rubro inmobiliario y de la construcción dan cuenta de nuestra calidad.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa fa-handshake-o"></i>
                                            <h3>Visión Estratégica</h3>
                                            <h4 class="subtitle"><span>Para Superar objetivos</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">Visión Estratégica</p>
                                            <p>No solo diseñamos su sitio web, lo ayudamos a armar un negocio en torno a él haciéndolo rentable.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa fa-map-marker"></i>
                                            <h3>Somos de Rosario</h3>
                                            <h4 class="subtitle"><span>Y llegamos al mundo</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">Estamos Cerca Tuyo</p>
                                            <p>Aunque hoy estamos exportando servicios a más de 12 países, nuestra base está en Rosario, lo cual nos permite vivir y entender el mercado Rosarino</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="one-fourth">
                            <div class="do-service-container">
                                <div class="do-service-container-inner">
                                    <div class="do-front-part">
                                        <div class="do-front-content color-box">
                                            <i class="fa  fa-thumbs-o-up"></i>
                                            <h3>Estamos Avalados</h3>
                                            <h4 class="subtitle"><span>Por Tokko Broker</span></h4>
                                        </div>
                                    </div>
                                    <div class="do-back-part">
                                        <div class="do-back-content color-box">
                                            <p class="service-title">Tokko Broker Partner</p>
                                            <p>Nuestro excelente trabajo en proyectos de Real Estate hace que empresas como Tokko Broker depositen su confianza en nosotros como proveedores de desarrollo web.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <br><br>
    <div class="align-center"><a href="#contact-chat" class="button border border-white large-btn"><i class="fa  fa-comments"></i> Contactanos</a></div>            
</section>


<section class="content bg-gray tokko-portals">
    <div class="container-center">
            <div class="section-title align-center">
                <h2>Aumente y simplifique la difusión de sus inmuebles</h2>
                <h3 class="sub-heading">Descuentos exclusivos para clientes de Tokko Broker</h3>
            </div>

            <div class="col-row">
                <ul class="tokko-portals">
                    <li><img src="/sites/all/themes/estimation/img/tokko/logo-argenprop.png" /></li>
                    <li><img src="/sites/all/themes/estimation/img/tokko/logo-mercadolibre.png" /></li>
                    <li><img src="/sites/all/themes/estimation/img/tokko/logo-reporteinmo.png" /></li>
                    <li><img src="/sites/all/themes/estimation/img/tokko/logo-zonaprop.svg" /></li>
                    <li><img src="/sites/all/themes/estimation/img/tokko/logo-properati.png" /></li>
                </ul>
            </div><!-- end col-row -->
    </div><!-- end container-center -->
</section><!-- end content -->

<section class="content tokko-chat">
    <div class="container-center">
        <div class="col-row">
            <div class="one-third"><i class="fa fa-comments"></i></div>
            <div class="two-third">
                <h2>Querés saber más acerca de nuestros servicios?</h2>
                <h3>Chateá en vivo con un representante.</h3>
                <br>
                <a href="#contact-chat" class="button large-btn"><i class="fa  fa-comments"></i> Contactanos</a>
            </div>
        </div>
    </div>
</section><!-- end content -->
  

<footer id="footer" class="content">
	<div class="footer-main container-center">
		<div class="col-row">
            <?php if ($page['f1'] || $page['f2'] || $page['f3'] || $page['f4']) : ?>
                <?php if ($page['f1']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f1']); ?>
                    </div><!-- end  -->
                <?php endif; ?> 
                <?php if ($page['f2']): ?>
                    <div class="one-fourth cf">
                        <?php print render($page['f2']); ?>
                    </div><!-- end  -->	
                <?php endif; ?> 

                <?php if ($page['f3']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f3']); ?>						
                    </div><!-- end  -->			
                <?php endif; ?> 

                <?php if ($page['f4']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f4']); ?>
                    </div><!-- end  -->					
                <?php endif; ?> 
            <?php endif; ?> 
		</div><!-- end col-row -->	
       <?php if($page['f5'] || $page['f6'] || $page['f7'] || $page['f8']) : ?>
		<div class="separator-1"></div><!-- end separator -->

		<div class="col-row">			
          <?php if ($page['f5']): ?>
			<div class="one-fourth">
				 <?php print render($page['f5']); ?>	
			</div>
         <?php endif; ?> 
          <?php if ($page['f6']): ?>
			<div class="one-fourth">
				 <?php print render($page['f6']); ?>
			</div>		
          <?php endif; ?> 
          <?php if ($page['f7']): ?>
			<div class="one-fourth">
            <?php print render($page['f7']); ?>
			</div>		
            <?php endif; ?>
            <?php if ($page['f8']): ?> 
			<div class="one-fourth cf">
			<?php print render($page['f8']); ?>
			</div>									
				<?php endif; ?> 
		</div><!-- end col-row -->
<?php endif; ?> 
	</div><!-- end footer-main -->

	<div class="footer-bottom container-center cf">					
			
		<div class="bottom-left">	
          <?php if ($page['footer-a']): ?>
          <?php print render($page['footer-a']); ?>
          <?php endif; ?>  								
							
		</div><!-- end bottom-left -->		
		
		<div class="bottom-right">
         <?php if ($page['footer-b']): ?>
         <?php print render($page['footer-b']); ?>
         <?php endif; ?>
			
		</div><!-- end bottom-right -->				
			
	</div><!-- end footer-bottom -->
</footer>
<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '</div><!-- end wrapper -->';
  }
  else {

  }
?> 
<div class="scroll-top"><a href="#"><i class="fa fa-angle-up"></i></a></div>
