<div id="node-<?php print $node->nid; ?>" class="main-content-left">
    <article class="blog-post cf">						
        <div class="mediaholder">
            <?php print render($content['field_media']); ?>
        </div>	<!-- end mediaholder -->
        <div class="meta">
            <div class="date">
                <span class="day"><?php print date('d', $created); ?></span>
                <span class="month"><?php print date('M', $created); ?></span>						
            </div><!-- end date -->
            <div class="blog-type"><?php print render($content['field_blog_type_icon']); ?></div><!-- end blog-type -->					
            <?php
            if ($page) {
                ?>
                <div class="share-post">
                    <ul class="share-networks">
                        <li><div id="twitter" data-url="URL" data-text="<?php print $title; ?>"></div></li>
                        <li><div id="facebook" data-url="<?php print base_path(); ?>" data-text="<?php print $title; ?>"></div></li>

                    </ul>		
                    <a class="share-btn closed" href="#">Share</a>
                </div><!-- share-post -->	
            <?php } ?>							
        </div><!-- end meta -->

        <div class="post-content">	
            <?php
                hide($content['comments']);
                hide($content['links']);
                print render($content);
            ?>		
            <?php
                if (!$page) :
                ?>	
                <a class="button small-btn" href="<?php print $node_url; ?>">
                    <?php print t('Continue Reading'); ?>
                </a> 
            <?php endif; ?>	
        </div><!-- end post-content -->
        <div class="clearfix"></div>
        <?php /*
            if ($page) :
                $node_author = user_load($node->uid);
            ?>
            <div class="blogPage">
                <div class="testimony boxshadow cf">
                    <div class="testimony-inner">
                        <img class="portrait" src="/sites/default/files/styles/110x110/public/pictures/<?php echo $node_author->picture->filename; ?>" >
                        <div class="testimony-author">
                            <h4><?php echo $node_author->field_full_name['und'][0]['value']; ?></h4>
                            <span><?php echo $node_author->field_position['und'][0]['value']; ?></span>						
                        </div>
                        <blockquote class="testimony-text">
                            <?php echo $node_author->field_philosophy['und'][0]['value']; ?>								
                        </blockquote>	
                        <!-- end testimony-author -->
                    </div><!-- end testimony -->
                </div>
            </div>
        <?php endif; */ ?>
    </article><!-- end blog-post -->						
</div><!-- end node -->
<div class="separator-1"></div><!-- end separator -->	
<section class="comments-section cf">
    <?php print render($content['comments']); ?>
</section><!-- end comments-section -->




