<div id="node-<?php print $node->nid; ?>" class="main-content-left">
    <article class="blog-post cf">						
        DDD
        <div class="mediaholder">
            <?php print render($content['field_media']); ?>
        </div>	<!-- end mediaholder -->
        <div class="meta">
            <div class="date">
                <span class="day"><?php print date('d', $created); ?></span>
                <span class="month"><?php print date('M', $created); ?></span>						
            </div><!-- end date -->
            <div class="blog-type">
                <?php print render($content['field_blog_type_icon']); ?>
            </div><!-- end blog-type -->	

        </div><!-- end meta -->
        <div class="post-content">	
            <div class="post-title">
                <h3>
                    <a href="<?php print $node_url; ?>">
                        <?php print $title; ?>
                    </a>
                </h3>
                <div class="tags">
                    <?php /*<span>
                        <i class="fa fa-user"></i>
                        <a href="#">
                            <?php print theme('username', array('account' => $node)); ?>
                        </a>
                    </span> */ ?>
                    <span>
                        <i class="fa fa-tags"></i>
                        <?php print render($content['field_tags']); ?>
                    </span>
                    <span>
                        <i class="fa fa-comments"></i>
                        <a href="#">
                            <?php print $comment_count; ?> <?php print t('comments'); ?>
                        </a>
                    </span>
                </div>
            </div><!-- end post-title -->

            <?php
            hide($content['comments']);
            hide($content['links']);
            print render($content);
            ?>				
            <?php
            if ($teaser) {
                ?>
                <a class="button small-btn" href="<?php print $node_url; ?>"><?php print t('Continue Reading'); ?></a> 
            <?php } ?>			
        </div><!-- end post-content -->							
    </article><!-- end blog-post -->
</div>