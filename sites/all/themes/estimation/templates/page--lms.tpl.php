<?php
if (theme_get_setting('estimation_preloader') == TRUE) {
    print '<!-- Preloader -->
<div id="preloader">
    <div id="status"></div>
</div>';
  }
  else {

  }
?> 

<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '<div class="wrapper">';
  }
  else {

  }
?> 

<header id="header">
	<div class="container-center cf">
      <div class="logo">
        <?php if($logo): ?>
          <a href="<?php print $front_page; ?>"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"></a>
        <?php else: ?>
          <h1 class="site-name"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
           <p class="tagline"><?php print $site_slogan; ?></p>
        <?php endif; ?>
       
      </div>
		<div class="userInfoBlock">
            <a href="#" class="userOpen openBlock"><i class="fa fa-user" aria-hidden="true"></i></a>
            <?php if ($page['user-login']): ?>
                <div class="userForm">
                    <?php print render($page['user-login']); ?>
                </div> <!-- /.section, /#sidebar-first -->
            <?php else: ?>
                <?php if (!empty($_SESSION['client'])): ?>
                    <div class="userForm">
                        <p class="logoutBlock"><a href="/logout" ><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Logout</a></p>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <span class="menuMobile"><i class="fa fa-bars" aria-hidden="true"></i></span>
		<div class="search-container">
			<a id="toggle-search" class="search-button search-open" href="#"><i class="fa fa-search"></i></a>
			<div class="search-panel cf">	
                <?php $block = module_invoke('search', 'block_view', 'search'); print render($block); ?>	  
			</div><!-- end search-panel -->
		</div><!-- search-container -->
		<nav id="main-navigation" class="navigation cf">
            <?php print render($page['main_menu']); ?>
		</nav>
	</div><!-- end container-center -->
</header>

<?php 
    $node = menu_get_object(); 
    if($node) {
        $node_type = $node->type;
    } else {
        $node_type = null;
    }
?>
<?php if (!drupal_is_front_page() && $node_type != 'article'): ?>
    <!-- #page-title start -->
    <section class="page-title lms section-bg-1">
        <div class="page-title-inner container-center cf">
            <div class="page-title-icon livechat-icon"><img src="/sites/all/themes/estimation/img/lms/init_isologo.png" alt="INIT Consultants"/></div>
            <h1>INIT <span>E-Learning Studio</span></h1>	
            <h2>We help you implement your online training solutions</h2>
        </div><!-- end container-center -->
    </section>
<?php endif; ?>

<section class="container-center lms video">
    <img src="/sites/all/themes/estimation/img/lms/screen.png" alt="E-Learning Video" />
</section>
    
<section class="container-center lms services">
	
	<div class="col-row">
		<div class="one-fourth">
			<div class="step">
				<div class="step-icon">
					<i class="fa fa-laptop"></i>
				</div><!-- end step-icon -->
				<span>Easy-to-use LMS System</span>
			</div><!-- end step -->
		</div>

		<div class="one-fourth">
			<div class="step">
				<div class="step-icon">
					<i class="fa fa-users"></i>
				</div><!-- end step-icon -->
				<span>Suport from our specialists to help you design your courses</span>
			</div><!-- end step -->
		</div>

		<div class="one-fourth">
			<div class="step">
				<div class="step-icon">
					<i class="fa fa-money"></i>
				</div><!-- end step-icon -->
				<span>Pricinig according to your needs and implementation</span>
			</div><!-- end step -->
		</div>

		<div class="one-fourth">
			<div class="step">
				<div class="step-icon">
					<i class="fa fa-line-chart"></i>
				</div><!-- end step-icon -->
				<span>Scalability according to your business demands</span>
			</div><!-- end step -->
		</div>
	</div><!-- end col-row -->
</section>

<div class="separator-1"></div>
    
<section class="lms why-chose-us">
    <div class="container-center">

        <div class="section-title align-center">
                <h2>Why Choose Us</h2>
                <h3 class="sub-heading">Vestibulum quam elit dapibus augue</h3>
        </div>	

        <div class="col-row">			

                <div class="one-half animated fadeInLeft" data-animation="fadeInLeft" style="opacity: 1;">
                        <h3>Our Vision</h3>				
                        <p>
                            At INIT we believe that a successful e-learning practice requires technological 
                            and educational tools that fit your organization's culture and possibilities. 
                            Our e-learning specialists will guide you through implementing a learning management 
                            system (LMS) and designing your courses, according to your company's interests. 
                            Together we can help your business reach the next level of success. 
                        </p>
                        			
                </div><!-- end one-half -->	

                <div class="one-half animated fadeInRight" data-animation="fadeInRight" style="opacity: 1;">
                    <h3>Our Team</h3>
                    <p>
                        We are a group of IT experts and e-learning specialists that work together to understand 
                        your company's obstacles and goals, and implement an online training system that suits 
                        your expectations and team capacities. Our e-learning studio combines educational and 
                        technological tools, with continuing support from our experts.
                    </p>
                    
                </div>

        </div><!-- end col-row -->	
    </div>
</section>

<section class="color-box-holder box-4 section-bg-1 services-section lms social-proof">
    <div class="arrow-white-down" style="margin-top: -52px;"></div>
    <div class="region region-content-no-wrap">
        <div id="block-block-26" class="block block-block contextual-links-region align-center">
            
            <h2 class="heading-extra-big text-white">THE FUTURE FOR STAFF TRAINING IS HERE</h2>
            
            <h3>INIT e-learning studio makes e-learning solutions easy, customized and accessible. </h3>
            <a href="#contact-chat" class="button border lms-btn large-btn"><i class="fa  fa-comments"></i> CONTACT US NOW and GET a FREE QUOTE</a>
            <br>
            <br>
        </div>
    </div>
</section>

    
<?php if ($node_type == 'article'): ?>
    <!-- #page-title start -->
    <?php $bg_image = file_create_url($node->field_cover[LANGUAGE_NONE][0]['uri']); ?>
    <?php 
        $cover_style = 'background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, rgba(0, 0, 0, .2)), color-stop(100%, rgba(0, 0, 0, .9))), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -webkit-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -moz-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: -o-linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background: linear-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .9)), url(' . $bg_image . ') center center no-repeat;'
            . 'background-size: cover;';
    ?>
    <section class="page-title section-bg-1 article-cover" style="<?php print $cover_style; ?>">
        <div class="page-title-inner container-center cf" >
            <h1 class="container-center"><?php print $title; ?></h1>
            <h4 class="article-subtitle"><?php print $node->field_article_subtitle[LANGUAGE_NONE][0]['value']; ?></h4>
            <?php
                if ($page) :
                    $node_author = user_load($node->uid);
                ?>
                <div class="article-author">
                    <img src="/sites/default/files/styles/110x110/public/pictures/<?php echo $node_author->picture->filename; ?>" >
                    <div>
                        <h4><?php echo $node_author->field_full_name['und'][0]['value']; ?></h4>
                        <span><?php echo $node_author->field_position['und'][0]['value']; ?></span>
                    </div>
                </div>
            <?php endif; ?>
        </div><!-- end container-center -->
    </section>
<?php endif; ?>
    
<?php if ($page['content_top']): ?>
    <section class="content-top">	
        <?php print render($page['content_top']); ?>
    </section>
<?php endif; ?>
    
<?php if ($page['slider']): ?>
    <section class="container-slider">	
        <div id="slider">	
            <?php print render($page['slider']); ?>
        </div><!-- end Slider -->   
    </section>
<?php endif; ?>

<?php if ($page['featured']): ?>
    <section class="content">
        <div class="container-center">
            <?php print render($page['featured']); ?>

        </div><!-- end container-center -->
    </section><!-- end content -->
<?php endif; ?> 
<?php if ($page['content_no_wrap']): ?>   
    <section class="color-box-holder box-4 section-bg-1 services-section">
        <div class="arrow-white-down" style="margin-top: -52px;"></div>
        <?php print render($page['content_no_wrap']); ?>
    </section>
<?php endif; ?> 
     
     


<section class="content container-center cf <?php if (drupal_is_front_page()): ?>
<?php
if (theme_get_setting('estimation_bg') == TRUE) {
    print 'section-bg-4';
  }
  else {

  }
?>
<?php endif; ?>
"> 

      <?php if ($page['sidebar']): ?>
        <div class="main-content-left">
      <?php endif; ?>
      <?php if (!$page['sidebar']): ?>
        <div class="no-sidebar">
      <?php endif; ?>

        <?php if ($messages): ?>
            <div id="messages">
                <?php print $messages; ?>
                <span class="btnHide">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </span>
            </div>
        <?php endif; ?>
         <?php if ($title): ?><h1 class="title" id="pt"><?php print $title; ?></h1><?php endif; ?>
                
        <?php if ($tabs): ?><div class="d-tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php print render($page['content']); ?>
		



</div><!-- end main-content -->

<?php if ($page['sidebar']): ?>
    <!-- sidebar first -->
    <div class="sidebar sb-right">
        <div class="sidebar-box">
            <?php print render($page['sidebar']); ?>
        </div>
    </div>
    <!-- // sidebar -->
<?php endif; ?>
</section><!-- end -->
   
<?php if ($page['after_bottom']): ?>	
    <section class="twitter-holder section-bg-7">
        <?php print render($page['after_bottom']); ?>	
    </section>
<?php endif; ?>

<?php if ($page['after_content']): ?>
    <section class="content">
        <div class="after-content">		
            <?php print render($page['after_content']); ?>
        </div>
    </section><!-- end -->
<?php endif; ?>

<?php if ($page['content_bottom']): ?>
    <section class="content light-section section-bg-1">
        <div class="container-center">		
            <div class="col-row">
                <?php print render($page['content_bottom']); ?>
            </div><!-- end col-row -->		
        </div><!-- end container-center -->
    </section><!-- end content -->
<?php endif; ?> 
    
	
<?php if($page['postscript_first'] || $page['postscript_second'] || $page['postscript_third'] || $page['postscript_fourth'] ) : ?>
    <section class="content">
        <div class="container-center">
            <?php if ($page['postscript_first']): ?>
                <?php print render($page['postscript_first']); ?>
            <?php endif; ?>
            <?php if ($page['postscript_second']): ?>
                <?php print render($page['postscript_second']); ?>
            <?php endif; ?>

            <?php if ($page['postscript_fourth']): ?>
                <div class="col-row">
                    <div class="one-half">
                            <?php print render($page['postscript_fourth']); ?>
                    </div>
                </div>
            <?php endif; ?>
            <!-- end col-row -->		
        </div><!-- end container-center -->
    </section><!-- end content -->	
<?php endif; ?>
<?php if ($page['postscript_third']): ?> 
    <div class="postscript_third">	
        <?php print render($page['postscript_third']); ?>
    </div>	
<?php endif; ?>
<?php if ($page['bottom']): ?>
    <section class="content">
        <div class="container-center">
            <?php print render($page['bottom']); ?>	
        </div>
    </section>
<?php endif; ?>


<footer id="footer" class="content">
	<div class="footer-main container-center">
		<div class="col-row">
            <?php if ($page['f1'] || $page['f2'] || $page['f3'] || $page['f4']) : ?>
                <?php if ($page['f1']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f1']); ?>
                    </div><!-- end  -->
                <?php endif; ?> 
                <?php if ($page['f2']): ?>
                    <div class="one-fourth cf">
                        <?php print render($page['f2']); ?>
                    </div><!-- end  -->	
                <?php endif; ?> 

                <?php if ($page['f3']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f3']); ?>						
                    </div><!-- end  -->			
                <?php endif; ?> 

                <?php if ($page['f4']): ?>
                    <div class="one-fourth">
                        <?php print render($page['f4']); ?>
                    </div><!-- end  -->					
                <?php endif; ?> 
            <?php endif; ?> 
		</div><!-- end col-row -->	
       <?php if($page['f5'] || $page['f6'] || $page['f7'] || $page['f8']) : ?>
		<div class="separator-1"></div><!-- end separator -->

		<div class="col-row">			
          <?php if ($page['f5']): ?>
			<div class="one-fourth">
				 <?php print render($page['f5']); ?>	
			</div>
         <?php endif; ?> 
          <?php if ($page['f6']): ?>
			<div class="one-fourth">
				 <?php print render($page['f6']); ?>
			</div>		
          <?php endif; ?> 
          <?php if ($page['f7']): ?>
			<div class="one-fourth">
            <?php print render($page['f7']); ?>
			</div>		
            <?php endif; ?>
            <?php if ($page['f8']): ?> 
			<div class="one-fourth cf">
			<?php print render($page['f8']); ?>
			</div>									
				<?php endif; ?> 
		</div><!-- end col-row -->
<?php endif; ?> 
	</div><!-- end footer-main -->

	<div class="footer-bottom container-center cf">					
			
		<div class="bottom-left">	
          <?php if ($page['footer-a']): ?>
          <?php print render($page['footer-a']); ?>
          <?php endif; ?>  								
							
		</div><!-- end bottom-left -->		
		
		<div class="bottom-right">
         <?php if ($page['footer-b']): ?>
         <?php print render($page['footer-b']); ?>
         <?php endif; ?>
			
		</div><!-- end bottom-right -->				
			
	</div><!-- end footer-bottom -->
</footer>
<?php
if (theme_get_setting('estimation_boxed') == TRUE) {
    print '</div><!-- end wrapper -->';
  }
  else {

  }
?> 
<div class="scroll-top"><a href="#"><i class="fa fa-angle-up"></i></a></div>