<?php
global $theme_root ;
$theme_root = base_path() . drupal_get_path('theme', 'estimation');



function estimation_breadcrumb($variables) {

  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {

   $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

	$breadcrumb[] = drupal_get_title();
    $output .= '<div class="breadcrumbs"><span>// You Are Here:</span> '  . implode('  /  ', $breadcrumb) . '</div>';
    return $output;
 }
}


function estimation_preprocess_html(&$vars) {

  $bgklasa = theme_get_setting('theme_bg_pattern');
  $vars['classes_array'][] = drupal_html_class($bgklasa);
  
  // The Color Palette.
  $file = theme_get_setting('theme_color_palette');
  drupal_add_css(path_to_theme() . '/css/skins/' . $file . '.css');
    
}
function estimation_preprocess_button(&$vars) {
  $vars['element']['#attributes']['class'][] = 'button medium-btn';
  
}


function estimation_preprocess_page(&$vars) {
  $alias_parts = explode('/', drupal_get_path_alias());	

  if (count($alias_parts) && $alias_parts[0] == 'web360') {

    $vars['theme_hook_suggestions'][] = 'page__web360';
  } else if(count($alias_parts) && $alias_parts[0] == 'tokko') {
    $vars['theme_hook_suggestions'][] = 'page__tokko';  
  } else if(count($alias_parts) && $alias_parts[0] == 'lms') {
    $vars['theme_hook_suggestions'][] = 'page__lms';    
  }
}
