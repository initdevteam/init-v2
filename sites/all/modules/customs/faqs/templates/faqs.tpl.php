<?php if (!empty($faqs)): ?>
    <div class="accordion">
        <?php foreach($faqs as $faq): ?>
            <h4 class=""><?php echo $faq->title; ?></h4>
            <div class="content">
                <?php echo $faq->field_answer['und'][0]['safe_value']; ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>