<?php if (!empty($content['map_image_bg'])): ?>
    <div class="mapBlock">
        <div class='container-center'>
            <?php if (!empty($content['map_image_bg_responsive'])) : ?>
                <img class="bg-map hide-xs" src="<?php echo $content['map_image_bg']; ?>" />
                <img class="bg-map imgResponsive" src="<?php echo $content['map_image_bg_responsive']; ?>" />
            <?php else: ?>
                <img class="bg-map" src="<?php echo $content['map_image_bg']; ?>" />
            <?php endif; ?>

            <?php if (!empty($content['countries_values'])): ?>
                <ul class="list">
                    <?php foreach ($content['countries_values'] as $country): ?>
                    <li>
                        <span class='imgFlag'><img src="/<?php echo drupal_get_path('module', 'map'); ?>/flags/blank.png" class="flag flag-<?php echo strtolower($country); ?>" alt="Czech Republic" /></span>
                        <span class='name'><?php echo $content['countries'][$country]; ?></span>
                    </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
